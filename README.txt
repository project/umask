

UMASK
=====

On PHP previous to version 5.2.5, move_uploaded_file() doesn't respect the
umask if the file is just moved (renamed). For details, see
http://bugs.php.net/bug.php?id=42291.

This module simply change the mode bits (chmod) to ~umask & 0666 for a file
just uploaded with CCK module FileField <http://drupal.org/project/filefield>.
The module doesn't (yet) support core's Upload module.

This module is developed by Thomas Barregren <http://drupal.org/user/16678> at
imBridge <http://drupal.org/node/131670>.

The development of this module has been sponsored by:

  * Dagens Industri <http://di.se/>
  * imBridge <http://www.imbridge.com/>

